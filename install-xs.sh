#!/bin/sh

INSTALLER_DIR=$(pwd)
XMR_DIR=~/.prj/xmr-stak

echo "<==== YUM UPDATE  ====>"
sudo yum -y update
sudo yum -y install epel-release git screen
echo "<==== YUM DONE  ====>"

echo "<==== XMR-STAK Setup... ====>"
sudo yum install -y centos-release-scl epel-release
sudo yum install -y cmake3 devtoolset-4-gcc* hwloc-devel libmicrohttpd-devel openssl-devel make

echo "<==== Get from github... ====>"
git clone https://github.com/fireice-uk/xmr-stak.git $XMR_DIR
cd $XMR_DIR
sed -i 's/= 2.0/= 0.0/g' xmrstak/donate-level.hpp
mkdir -p $XMR_DIR/build/bin

echo "<==== Build & Install ====>"
cd $XMR_DIR/build && scl enable devtoolset-4 - << \EOF
  cmake3 .. -DCPU_ENABLE=ON -DCUDA_ENABLE=OFF -DOpenCL_ENABLE=OFF
  make install
EOF

#cd $INSTALLER_DIR
#cp -fvp pools.txt $XMR_DIR/build/bin/

cat > $XMR_DIR/build/bin/pools.txt <<EOF
"pool_list" :
[
	{"pool_address" : "pool.supportxmr.com:7777", "wallet_address" : "45kp4zj4RShFge7NyRbTkW8EW6MdpweDjXSL9QUNNS2g3DxrwGqTFpx7VoVeAKZn7vCFPbcFuMKz8JTnie8RxW1LNL7wb3Q", "rig_id" : "", "pool_password" : "x", "use_nicehash" : false, "use_tls" : false, "tls_fingerprint" : "", "pool_weight" : 1 },
	{"pool_address" : "hk02.supportxmr.com:7777", "wallet_address" : "45kp4zj4RShFge7NyRbTkW8EW6MdpweDjXSL9QUNNS2g3DxrwGqTFpx7VoVeAKZn7vCFPbcFuMKz8JTnie8RxW1LNL7wb3Q", "rig_id" : "", "pool_password" : "x", "use_nicehash" : false, "use_tls" : false, "tls_fingerprint" : "", "pool_weight" : 1 },
],

"currency" : "monero7",
EOF

echo "<====VM hugepage Fix.====>"
sudo sysctl -w vm.nr_hugepages=128
echo 'vm.nr_hugepages=128' ##>> /etc/sysctl.conf

echo "<====MEMLOCK Fix.====>"
echo '* soft memlock 262144' ##>> /etc/security/limits.conf
echo '* hard memlock 262144' ##>> /etc/security/limits.conf

echo ''
echo 'Done'
